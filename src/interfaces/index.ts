export interface ProductTypes {
  id: number;
  product: string;
  calories: number;
  fat: number;
  carbs: number;
  protein: number;
  iron: number;
  [key: string]: any;
}

export interface ColumnTypes {
  id: number;
  title: string;
  key: string;
}
