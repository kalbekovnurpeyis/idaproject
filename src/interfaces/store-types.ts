import { ProductTypes, ColumnTypes } from ".";

export interface AlertTypes {
  message: string;
  type: string;
  visibility: boolean;
}

export interface RootState {
  columns: ColumnTypes[];
  products: ProductTypes[];
  selectedRowKeys: number[];
  sortByColumn: string;
  orderByDesc: boolean;
  alert: AlertTypes | null;
  perPage: number;
  pageIndex: number;
}
