import Vue from "vue";
import Vuex, { ActionTree, GetterTree, MutationTree } from "vuex";

import { getProducts, deleteProducts } from "@/api/request";
import columns from "@/api/columns.json";
import { RootState } from "@/interfaces/store-types";
import { ColumnTypes, ProductTypes } from "@/interfaces";
import compareValues from "@/helpers/compare-values";

Vue.use(Vuex);

const state: RootState = {
  columns,
  products: [],
  selectedRowKeys: [],
  sortByColumn: localStorage.getItem("sortByColumn") || columns[0].key,
  orderByDesc: false,
  alert: null,
  perPage: 10,
  pageIndex: 0
};

const getters: GetterTree<RootState, RootState> = {
  orderedColumns(state): ColumnTypes[] {
    const selectedColumn: ColumnTypes =
      state.columns.find(column => column.key === state.sortByColumn) ||
      state.columns[0];

    const otherColumns = selectedColumn
      ? state.columns.filter(column => column.key !== selectedColumn.key)
      : [];

    return selectedColumn ? [selectedColumn, ...otherColumns] : otherColumns;
  },

  sortedData: (state): ProductTypes[] => {
    return state.products
      .sort(compareValues(state.sortByColumn, state.orderByDesc))
      .slice(state.pageIndex, state.pageIndex + state.perPage);
  }
};

const mutations: MutationTree<RootState> = {
  setSelectedRows: (state, data) => {
    state.selectedRowKeys = data;
  },

  changeOrder(state) {
    state.orderByDesc = !state.orderByDesc;
  },

  setSortColumn(state, data) {
    state.sortByColumn = data;
  },

  addColumnList(state, data: number) {
    const column = columns.find(item => item.id === data);

    if (column && !state.columns.find(item => item.id === column.id)) {
      state.columns = [...state.columns, column].sort(
        (columnPrev, columnNext) => columnPrev.id - columnNext.id
      );
    }
  },

  removeColumnList(state, data: number) {
    state.columns = state.columns.filter(column => column.id !== data);
  },

  setProducts(state, products: ProductTypes[]) {
    state.products = products;
  },

  removeProduct(state, id: number) {
    state.products = state.products.filter(product => product.id !== id);
  },

  setAlert(state, alert) {
    setTimeout(() => {
      state.alert = alert;
    }, 10);

    setTimeout(() => {
      state.alert = { ...alert, visibility: false };
    }, 3000);
  },

  setPerPage(state, count: number) {
    state.perPage = count;
  },

  setPageIndex(state, index: number) {
    state.pageIndex = index;
  }
};

const actions: ActionTree<RootState, RootState> = {
  async fetchProducts({ commit }) {
    try {
      const response = await getProducts();
      commit("setProducts", response);
    } catch ({ error }) {
      commit("setAlert", { message: error, type: "error", visibility: true });
    }
  },

  async removeProducts({ commit }, products: number | number[]) {
    try {
      const response: any = await deleteProducts();

      commit("setAlert", {
        message: response.message,
        type: "success",
        visibility: true
      });

      if (typeof products === "number") {
        commit("removeProduct", products);
      } else {
        products.forEach(item => commit("removeProduct", item));
      }
    } catch ({ error }) {
      commit("setAlert", { message: error, type: "error", visibility: true });
    }
  },

  onSelectAllRows: ({ commit }, { selected, value }): void => {
    if (selected) {
      commit(
        "setSelectedRows",
        (value === -1 ? state.products : value).map(
          (data: ProductTypes) => data.id
        )
      );
    } else {
      commit("setSelectedRows", []);
    }
  },

  onRowSelect({ commit }, { value, selected = false }): void {
    commit(
      "setSelectedRows",
      selected
        ? [...state.selectedRowKeys, value]
        : state.selectedRowKeys.filter(item => item !== value)
    );
  },

  onSortColumnChange({ commit }, { key }) {
    localStorage.setItem("sortByColumn", key);
    commit("setSortColumn", key);
  },

  onColumnsListChange({ commit, dispatch }, { value, selected = false }) {
    if (selected) {
      commit("addColumnList", value);
    } else {
      commit("removeColumnList", value);

      // If sortByColumn removed, then choose first element of newColumn as sortByColumn
      if (!state.columns.find(data => data.key === state.sortByColumn)) {
        const column = state.columns[0] || columns[0];

        dispatch("onSortColumnChange", { key: column.key });
      }
    }
  }
};

export default new Vuex.Store<RootState>({
  state,
  getters,
  mutations,
  actions
});
