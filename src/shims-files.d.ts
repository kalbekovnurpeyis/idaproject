/**
 * Declaration for JPEG files
 */
declare module '*.jpeg' {
  const value: string;

  export default value;
}

/**
 * Declaration for JPG files
 */
declare module '*.jpg' {
  const value: string;

  export default value;
}

/**
 * Declaration for PNG files
 */
declare module '*.png' {
  const value: string;

  export default value;
}

/**
 * Declaration for SVG files
 */
declare module '*.svg' {
  const value: string;

  export default value;
}
