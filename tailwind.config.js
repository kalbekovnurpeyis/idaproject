module.exports = {
  purge: [],
  theme: {
    extend: {
      fontFamily: {
        body: ['Source Sans Pro', 'sans-serif'],
      },
      colors: {
        'c-black': {
          100: '#333333',
          200: '#282136',
        },
        'c-gray': {
          100: '#F8F9FA',
          200: '#EDEDED',
          300: '#C6CBD4',
        },
        'c-green': '#00A11E',
      },
      opacity: {
        '07': '0.07',
      },
    },
  },
  variants: {
    backgroundColor: ['responsive', 'even', 'odd', 'hover'],
    fontWeight: ['hover', 'group-hover'],
    padding: ['hover', 'group-hover'],
    margin: ['responsive', 'first', 'last'],
  },
  plugins: [],
};
